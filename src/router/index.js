import Vue from "vue";
import VueRouter from "vue-router";

//normal load
import AboutPage from "../pages/AboutPage";
import EducationPage from "../pages/EducationPage";
import WorkExperiencePage from "../pages/WorkExperiencePage";
import ProjectPage from "../pages/ProjectsPage";
import ErrorPage from "../pages/ErrorPage";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/about',
  },
  {
    path: '/about',
    component: AboutPage
  },
  {
    path: "/education",
    component: EducationPage
  },
  {
    path: "/workexperience",
    component: WorkExperiencePage
  },
  {
    path: "/projects",
    component: ProjectPage
  },
  {
    path: '*',
    component: ErrorPage
  }
];

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 }
  }
});

export default router;
