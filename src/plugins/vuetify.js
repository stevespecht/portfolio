import Vue from "vue";
import Vuetify from "vuetify/lib";
import colors from "vuetify/lib/util/colors";

Vue.use(Vuetify);

export default new Vuetify({
  //   theme: {
  //     themes: {
  //       light: {
  //         primary: "#7B6D8D",
  //         secondary: "#8499B1",
  //         accent: "#8499B1",
  //         error: colors.red.accent3,
  //       },
  //       dark: {
  //         primary: colors.blue.lighten3,
  //       },
  //     },
  //   },
  // theme: {
  //   themes: {
  //     light: {
  //       primary: "#79C7C5",
  //       secondary: "#b582af",
  //       accent: "#8499B1",
  //       error: colors.red.accent3,
  //     },
  //     dark: {
  //       primary: "#79C7C5",
  //       secondary: "#b582af",
  //       accent: "#8499B1",
  //       error: colors.red.accent3,
  //     },
  //   },
  // },
  theme: {
    themes: {
      light: {
        primary: "#40c9ff",
        secondary: "#b582af",
        accent: "#8499B1",
        error: colors.red.accent3,
      },
      dark: {
        primary: "#40c9ff",
        secondary: "#317691",
        accent: "#8499B1",
        error: colors.red.accent3,
      },
    },
  },
});
