//Observe elements, if they are intersecting add the 'enter' class and stop observing them
const elementVisibleObserver = new IntersectionObserver(
    (entries, elementVisibleObserver) => {
        entries.forEach((entry) => {
            if (entry.isIntersecting) {
                entry.target.classList.add('visible');
                elementVisibleObserver.unobserve(entry.target);
            }
        });
    }
);

export default {
    bind(el) {
        el.classList.add('notVisible');
        elementVisibleObserver.observe(el);
    }
}